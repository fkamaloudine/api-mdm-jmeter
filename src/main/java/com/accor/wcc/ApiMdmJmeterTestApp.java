package com.accor.wcc;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringJoiner;

public class ApiMdmJmeterTestApp extends AbstractJavaSamplerClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiMdmJmeterTestApp.class);

    @Override
    public Arguments getDefaultParameters() {
        Arguments args = new Arguments();
        args.addArgument("action", "");
        args.addArgument("args1", "");
        args.addArgument("args2", "");
        args.addArgument("args3", "");
        return args;
    }

    @Override
    public SampleResult runTest(JavaSamplerContext context) {
        String action = context.getParameter("action");
        String args1 = context.getParameter("args1");
        String args2 = context.getParameter("args2");
        String args3 = context.getParameter("args3");

        String label = action + " " + new StringJoiner(",", "[", "]").add(args1).add(args2).add(args3);
        LOGGER.info(label);

        SampleResult result = new SampleResult();
        result.setSampleLabel(action);
        result.setDataType(SampleResult.TEXT);
        result.sampleStart();

        boolean status = MDMClientApp.launch(new String[]{action, args1, args2, args3});

        result.setSuccessful(status);
        result.setResponseCodeOK();
        result.setResponseMessage("Request successful");

        result.sampleEnd();
        return result;
    }
}
